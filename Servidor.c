#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<unistd.h>
short SocketCreate(void)
{
    short hSocket;
    printf("******CLIENTE*****\n");
    printf("***PUERTO: 8586***\n\n");
    
    printf("Socket Creado\n");
    hSocket = socket(AF_INET, SOCK_STREAM, 0);
    return hSocket;
}
int BindCreatedSocket(int hSocket)
{
    int iRetval=-1;
    int ClientPort = 8586;
    struct sockaddr_in  remote= {0};
    /* Internet*/
    remote.sin_family = AF_INET;
    
    remote.sin_addr.s_addr = htonl(INADDR_ANY);
    remote.sin_port = htons(ClientPort); /* Puerto local */
    iRetval = bind(hSocket,(struct sockaddr *)&remote,sizeof(remote));
    return iRetval;
}
int main(int argc, char *argv[])
{
    int socket_desc, sock, clientLen, read_size;
    struct sockaddr_in server, client;
    char client_message[200]= {0};
    char message[100] = {0};
    const char *pMessage = "https://bitbucket.org/david_guada/sockets_en_c/";
    //Socket creado
    socket_desc = SocketCreate();
    if (socket_desc == -1)
    {
        printf("No se pudo crear el socket");
        return 1;
    }
    printf("Socket creado\n");
    //Enlace
    if( BindCreatedSocket(socket_desc) < 0)
    {
        
        perror("Enlace fallido.");
        return 1;
    }
    printf("Enlace exitoso \n");
    //En escucha
    listen(socket_desc, 3);
    //Aceptacion 
    while(1)
    {
        printf("Esperando conexion con el cliente...\n");
        clientLen = sizeof(struct sockaddr_in);
        //Conexion aceptada del cliente
        sock = accept(socket_desc,(struct sockaddr *)&client,(socklen_t*)&clientLen);
        if (sock < 0)
        {
            perror("Fallido");
            return 1;
        }
        printf("Conexion exitosa...\n");
        memset(client_message, '\0', sizeof client_message);
        memset(message, '\0', sizeof message);
        //Respuesta del cliente
  
        if( recv(sock, client_message, 200, 0) < 0)
        {
            printf("Respuesta Fallido");
            break;
        }
        printf("Respuesta del cliente: %s\n",client_message);
        if(strcmp(pMessage,client_message)==0)
        {
            strcpy(message,"¡HOLAAA!");
        }
        else
        {
            strcpy(message,"Mensaje recibido. Adios...");
        }
       
        if( send(sock, message, strlen(message), 0) < 0)
        {
            printf("Envio fallido");
            return 1;
        }
        close(sock);
        sleep(1);
    }
    return 0;
}