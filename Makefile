
CC=gcc
CFLAGS=-Wall -g
main: Servidor.c Cliente.o
		$(CC) $(CFLAGS) -o main Servidor.c Cliente.o

Cliente.o: Cliente.c
		$(CC) $(CFLAGS) -o Cliente.o Cliente.c

.PHONY: clean
clean:
		rm -rf *.o
